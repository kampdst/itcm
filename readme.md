KampDST DOIT ITCM
=================

KampDST's server infrastructure is managed using Ansible. The following resources define the infrastructure and how it should be configured. For the most part, the servers are split into `games servers` or `system servers`. 

`game servers` are the hosts that actually run the KampDST dedicated servers for players to join. `system servers` are used for system management such as backups, logs, and administration.


Current Systems
---------------

- `usatl.waxwell` System Task Runner
- `usatl.wendy` Game Server for `kdst101`
- `usatl.wortox` Game Server for `kdst101`


Planned Systems
---------------

See `kampdst/planning`.


Bootstrapping
-------------

Using `Linode`, bootstrapped hosts use `root` and the `id_rsa_kampdst_doit` ssh public key.

``` bash
ansible-playbook -i inventories/live --ask-vault-pass --user root --limit <new-host> core.yml
```


Provisioning
------------

Ideally, provision the entire stack at a time.

``` bash
ansible-playbook -i inventories/live --ask-vault-pass --user doit core.yml

```